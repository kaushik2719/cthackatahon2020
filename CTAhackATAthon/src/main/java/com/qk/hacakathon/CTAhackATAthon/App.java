package com.qk.hacakathon.CTAhackATAthon;

import org.junit.runners.BlockJUnit4ClassRunner;

import com.qk.hacakathon.CTAhackATAthon.core.Notifier;
import com.qk.hacakathon.CTAhackATAthon.script.ChallengeOne;
import com.qk.hacakathon.CTAhackATAthon.script.ChallengeZero;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[] args) {
		try {

			new BlockJUnit4ClassRunner(ChallengeOne.class).run(new Notifier());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
	}

}
