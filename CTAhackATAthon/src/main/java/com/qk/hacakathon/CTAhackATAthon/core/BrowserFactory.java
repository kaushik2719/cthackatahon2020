package com.qk.hacakathon.CTAhackATAthon.core;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BrowserFactory {

	private static final Logger LOG = LoggerFactory.getLogger(BrowserFactory.class);

	public static WebDriver createNewDriver(String browserType, String version, boolean remoteWebDriver)
			throws Exception {
		String pwd = new File(".").getAbsolutePath();
		WebDriver driver = null;
		// Browser
		switch (browserType) {
		case BrowserType.CHROME:
			LOG.info("Creating CHROME webdriver");
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.setExperimentalOption("prefs", new HashMap<String, Object>() {
				private static final long serialVersionUID = 1L;

				{
					put("profile.default_content_settings.popups", 0);
					put("download.default_directory", "/home/selenium/Downloads");
					put("download.prompt_for_download", false);
					put("download.directory_upgrade", true);
					put("safebrowsing.enabled", false);
					put("plugins.always_open_pdf_externally", true);
					put("plugins.plugins_disabled", new ArrayList<String>() {
						private static final long serialVersionUID = 1L;

						{
							add("Chrome PDF Viewer");
						}
					});
				}
			});
			LoggingPreferences logPrefs = new LoggingPreferences();
			logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
			logPrefs.enable(LogType.BROWSER, Level.SEVERE);
			chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			chromeOptions.setCapability("goog:loggingPrefs", logPrefs);
			chromeOptions.setCapability("enableVideo",true);
			chromeOptions.setCapability("enableVNC", true);
			
			if (version != null && remoteWebDriver) {
				chromeOptions.setCapability(CapabilityType.VERSION, version);
			}
			System.setProperty("webdriver.chrome.driver",
					pwd.substring(0, pwd.length() - 1) + "\\src\\main\\resource\\chromedriver.exe");
			driver = new ChromeDriver(chromeOptions);
			break;
		case BrowserType.IEXPLORE:
			System.setProperty("webdriver.ie.driver",
					pwd.substring(0, pwd.length() - 1) + "\\src\\main\\resource\\IEDriverServer.exe");
			LOG.info("Creating INTERNET_EXPLORER webdriver");
			InternetExplorerOptions explorerOptions = new InternetExplorerOptions();
			explorerOptions.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			explorerOptions.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			explorerOptions.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, false);
			explorerOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			if (version != null && remoteWebDriver) {
				explorerOptions.setCapability(CapabilityType.VERSION, version);
			}
			driver = new InternetExplorerDriver(explorerOptions);
			break;
		default:
			break;
		}
		driver.manage().window().maximize();
		return driver;
	}

}