package com.qk.hacakathon.CTAhackATAthon.core;

import java.util.Map;

import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunListener.ThreadSafe;
import org.junit.runner.notification.RunNotifier;
import org.openqa.selenium.JavascriptExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Notifier extends RunNotifier {

	private static final Logger LOG = LoggerFactory.getLogger(Notifier.class);

	public Notifier() {
		addFirstListener(new Listener());
	}

	@Override
	public void fireTestFailure(Failure failure) {
		super.fireTestFailure(failure);
		pleaseStop();
	}

	@ThreadSafe
	private class Listener extends RunListener {

		@Override
		public void testStarted(Description description) throws Exception {
			LOG.info("testStarted :{}", description.getMethodName());
		}

		@Override
		public void testFinished(Description description) throws Exception {
			LOG.info("testFinished :{}", description.getMethodName());
		}

		@Override
		public void testFailure(Failure failure) throws Exception {
			LOG.error("testFailure :{}", failure.getException());
		}

		@Override
		public void testIgnored(Description description) throws Exception {
			LOG.info("testIgnored :{}", description.getMethodName());
		}
	}

	/*
	 * @SuppressWarnings("unchecked") void fetchNavigationTimings() { try { Object
	 * rObj = ((JavascriptExecutor) Context.getDriver())
	 * .executeScript("return window.performance.getEntriesByType(\"navigation\")[0].duration;"
	 * ); System.err.println(rObj); Map<String, Object> timimgs = (Map<String,
	 * Object>) ((JavascriptExecutor) TestContext.getDriver())
	 * .executeScript("return window.performance.timing;");
	 * System.err.println(timimgs); } catch (Throwable e) {
	 * LOG.error("fetchNavigationTimings", e); } }
	 */
}
