package com.qk.hacakathon.CTAhackATAthon.script;

import java.util.List;

import org.jsoup.select.Elements;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qk.hacakathon.CTAhackATAthon.core.BrowserFactory;



public class ChallengeOne {
	
	
	
	@Test
	public void test01ProblemOne() throws Exception
	{
		WebDriver driver = BrowserFactory.createNewDriver(BrowserType.CHROME, null, false);
		
		driver.get("https://cpsatexam.org/");
		
		new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@class='slideout-toggle menu-item-align-right ']")));
		driver.findElement(By.xpath("//li[@class='slideout-toggle menu-item-align-right ']")).click();
		new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@href,'challenge-1')]")));
		driver.findElement(By.xpath("//a[contains(@href,'challenge-1')]")).click();
		new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(By.className("eicon-close")));
		driver.findElement(By.className("eicon-close")).click();
		
		List<WebElement> elements = 	driver.findElements(By.className("eael-accordion-list"));
		
		for (WebElement element : elements) {
			
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(element));
			element.click();
			
			String[] textSplit = element.getText().split("\\)");
			String text = textSplit[1];
			
			driver.get("https://translate.google.com");
			
			driver.findElement(By.id("source")).sendKeys(text);
			
			String translatedText = driver.findElement(By.className("tlid-translation translation")).getText();
			System.out.println(translatedText);
			
			driver.close();
			
			
		}
		
		
		
		
	}
}