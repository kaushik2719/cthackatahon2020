package com.qk.hacakathon.CTAhackATAthon.script;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;


public class ChallengeZero {

	@Test
	public void test01ProblemA() throws IOException {
		String apiURL = "http://35.188.114.237:8088/api/v2/users";
		String response = Jsoup.connect(apiURL).ignoreContentType(true).execute().body();
		// WSystem.out.println(response);
		JSONObject object = new JSONObject(response);
		JSONArray user = object.getJSONArray("user");

		for (int i = 0; i < user.length(); i++) {
			String id = user.getJSONObject(i).getString("id");
			System.out.println(id);
		}

		assertTrue(Jsoup.connect(apiURL).ignoreContentType(true).execute().statusCode() == 200);
	}

	@Test
	public void test02ProblemB() throws IOException, UnirestException {
		String addUrl = "http://35.188.114.237:8088/api/v2/addusers";

		Unirest.setTimeouts(0, 0);
		HttpResponse<String> response = Unirest.post(addUrl).header("Content-Type", "application/json")
				.body("{\r\n\"name\":\" Natasha\",\r\n\"surname\":\"Moraes\",\r\n\"adress\":\"Mumbai\"\r\n}")
				.asString();

		assertTrue(response.getStatus() == 200);

		JSONObject obj = new JSONObject(response.getBody());

		String id = obj.getString("id");

		System.out.println("id : " + id);

		String updateUrl = "http://35.188.114.237:8088/api/v2/updateuser";

		response = Unirest.put(updateUrl).header("Content-Type", "application/json").body(
				"{\r\n\"id\":" + id + ",\r\n\"name\":\"Nattu\",\r\n\"surname\":\"P\",\r\n\"adress\":\"New York\"\r\n}")
				.asString();

		assertTrue(response.getStatus() == 200);

		String deleteUrl = "http://35.188.114.237:8088/api/v2/" + id + "/deleteuser";

		response = Unirest.delete(deleteUrl).header("Content-Type", "application/json").asString();

		assertTrue(response.getStatus() == 200);

		String negativeTestUrl = "http://35.188.114.237:8088/api/v2/1000/deleteuser";

		response = Unirest.delete(negativeTestUrl).header("Content-Type", "application/json").asString();

		assertFalse(response.getStatus() == 200);
		assertTrue(response.getStatus() == 404);
	}
	
	@Test
	public void test03ProblemC() throws IOException, UnirestException {
		String addUrl = "http://35.188.114.237:8088/api/v2/addusers";

		Unirest.setTimeouts(0, 0);
		HttpResponse<String> response = Unirest.post(addUrl).header("Content-Type", "application/json")
				.body("{\r\n\"name\":\" Karthik\",\r\n\"surname\":\"S\",\r\n\"adress\":\"Mumbai\"\r\n}")
				.asString();
		
		JSONObject obj = new JSONObject(response.getBody());

		String id = obj.getString("id");
		
		String responseBody = Jsoup.connect("https://cpsatexam.org/index.php/challenge-0/").ignoreContentType(true).execute().body();
		
		String deleteUrl = "http://35.188.114.237:8088/api/v2/" + id + "/deleteuser";

		response = Unirest.delete(deleteUrl).header("Content-Type", "application/json").asString();
		
		Document doc = Jsoup.connect("https://cpsatexam.org/index.php/challenge-0/").ignoreContentType(true).execute().parse();
		
		String table = doc.getElementsByTag("table").html();
		
		if(!table.contains("<td>"+id+"<td>"))
		{
			System.out.println("ID deleted");
		}		
		
		assertTrue(response.getStatus() == 200);
	}

}
